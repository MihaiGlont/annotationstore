/**
 * Copyright (C) 2010-2014 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.model

import grails.persistence.Entity

@Entity
class PublicationLinkProvider implements Serializable {
    enum LinkType {
        PUBMED(PUBMED_LABEL),
        DOI(DOI_LABEL),
        /* comment out until we fix the integration with the annotation UI.
        ARXIV,
        ISBN,
        ISSN,
        JSTOR,
        NARCIS,
        NBN,
        PMC,
        */
        CUSTOM(CUSTOM_LABEL),
        MANUAL_ENTRY(MANUAL_LABEL)

        final String label
        static final String PUBMED_LABEL = "PubMed ID"
        static final String DOI_LABEL = "DOI"
        static final String CUSTOM_LABEL = "Other Link (URL)"
        static final String MANUAL_LABEL = "Publication without link"

        private LinkType(String linkLabel) {
            this.label = linkLabel
        }

        static LinkType findLinkTypeByLabel(String label) {
            switch (label) {
                case PUBMED_LABEL: return PUBMED
                case DOI_LABEL: return DOI
                case CUSTOM_LABEL: return CUSTOM
                case MANUAL_LABEL: return MANUAL_ENTRY
                default:
                    String msg = "No Publication Provider with label $label found.".toString()
                    throw new IllegalArgumentException(msg)
            }
        }
    }
    /* Type of the link */
    LinkType linkType
    /* Regular expression for the identifier */
    String pattern
    /* Identifiers.org prefix, if it exists */
    String identifiersPrefix

    static constraints = {
        linkType(nullable: false, unique: true)
        pattern(nullable: false)
        identifiersPrefix(nullable: true)
    }
}
